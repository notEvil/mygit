import rich.style as r_style
import subprocess_shell
import textual.app as t_app
import textual.binding as t_binding
import textual.screen as t_screen
import textual.widgets as t_widgets
import textual.widgets._directory_tree as tw__directory_tree
import textual.widgets._tree as tw__tree
import typer
import typing_extensions
import enum
import pathlib
import re
import sys
import typing


PATH = pathlib.Path(__file__).parent


import __main__

if __main__.__file__.endswith("/mygit.py"):
    mygit = __main__

else:
    append_path = str(PATH) not in sys.path
    if append_path:
        sys.path.append(str(PATH))

    import mygit

    if append_path:
        sys.path.remove(str(PATH))


def main(
    ignore_temporaries: typing_extensions.Annotated[
        bool, typer.Option(help="Ignore temporary files")
    ] = True,
    _code_offset: typing.Annotated[int, typer.Option(hidden=True)] = 0,
):
    """
    Track/untrack files with a convenient UI
    """

    repository_path = mygit._get_repo_path(None, _code_offset + 2).resolve()

    track_app = _TrackApp(repository_path, ignore_temporaries)
    if track_app.run() is None:
        sys.exit(_code_offset + 3)

    file_states = typing.cast(_DirectoryTree, track_app._directory_tree)._file_states
    current_file_states = _get_file_states(repository_path)

    add_paths = {
        path
        for path, file_state in file_states.items()
        if file_state == _FileState.ADDED
        and current_file_states.get(path) == _FileState.UNTRACKED
    }
    remove_paths = {
        path
        for path, file_state in current_file_states.items()
        if file_state == _FileState.ADDED
        and file_states.get(path) == _FileState.UNTRACKED
    }

    if len(add_paths) == 0 and len(remove_paths) == 0:
        if _code_offset == 0:
            mygit.print_stderr("Nothing to do", style=mygit._SUCCESS_STYLE)
        return

    print()
    for path in sorted(add_paths):
        mygit.print(" + ", end="", style="green")
        print(path.relative_to(repository_path))

    for path in sorted(remove_paths):
        mygit.print(" - ", end="", style="red")
        print(path.relative_to(repository_path))

    print()
    if len(add_paths) != 0:
        string = "file" if len(add_paths) == 1 else "files"
        print(f"track {len(add_paths)} {string}")

    if len(remove_paths) != 0:
        string = "file" if len(remove_paths) == 1 else "files"
        print(f"untrack {len(remove_paths)} {string}")

    print()
    while True:
        input_string = input("Apply [y(es), n(o)]? ")
        match input_string:
            case "y":
                break

            case "n":
                return False

    if len(add_paths) != 0:
        mygit.print_stderr("! track files", style=mygit._WARNING_STYLE)

        _v_ = "--pathspec-from-file=-"
        _v_ = ["git", "add", "--intent-to-add", _v_, "--pathspec-file-nul"]
        process = mygit._process(_v_)

        for path in add_paths:
            _ = process >> subprocess_shell.write(f"{path}\0".encode())

        _ = process >> subprocess_shell.wait()

    if len(remove_paths) != 0:
        mygit.print_stderr("! untrack files", style=mygit._WARNING_STYLE)

        _v_ = ["git", "reset", "--pathspec-from-file=-", "--pathspec-file-nul"]
        process = mygit._process(_v_)

        for path in remove_paths:
            _ = process >> subprocess_shell.write(f"{path}\0".encode())

        _ = process >> subprocess_shell.wait()


class _TrackApp(t_app.App):
    CSS = """
_DirectoryTree > .directory-tree--hidden {
  color: $text;
}

_MessageScreen {
  align: center middle;
}

_MessageScreen > Label {
  border: solid white;
  padding: 0 1;
}

.error {
  border: heavy red;
}
    """

    def __init__(self, path, ignore_temporaries):
        super().__init__()

        self.path = path
        self.ignore_temporaries = ignore_temporaries

        self._directory_tree = None
        self._search_input = None
        self._footer = None
        self._search_tuples = []

    def compose(self):
        self._directory_tree = _DirectoryTree(self)
        self._directory_tree.guide_depth = 3
        yield self._directory_tree

        self._footer = t_widgets.Footer()
        yield self._footer

    def start_search(self):
        if self._search_input is None:
            self._search_input = _SearchInput(self._search_tuples)
            self.mount(self._search_input)

        self._search_input.focus()

    def end_search(self, pattern, untrack):
        typing.cast(_SearchInput, self._search_input).remove()
        self._search_input = None

        if pattern is None:
            return

        typing.cast(_DirectoryTree, self._directory_tree).search(pattern, untrack)

        tuple = (pattern, untrack)
        if tuple not in self._search_tuples:
            self._search_tuples.append((pattern, untrack))

    def show_info(self, string):
        self.push_screen(_MessageScreen(string, False))

    def show_error(self, string):
        self.push_screen(_MessageScreen(string, True))


class _FileState(enum.Enum):
    ADDED = enum.auto()
    DELETED = enum.auto()
    MODIFIED = enum.auto()
    UNCHANGED = enum.auto()
    UNTRACKED = enum.auto()


class _DirectoryTree(t_widgets.DirectoryTree):
    _v_ = {_FileState.ADDED: "green", _FileState.MODIFIED: "dark_orange"}
    _COLOR_NAMES = typing.cast(dict[_FileState | None, str], _v_)

    _v_ = "continue()"
    BINDINGS = [
        ("tab", "toggle_node()", "Expand/Collapse"),
        ("space", "select_cursor()", "Track/Untrack"),
        t_binding.Binding("enter", _v_, description="Continue", key_display="ENTER"),
        ("/", "search()", "Search"),
    ]

    def __init__(self, track_app):
        self._path = track_app.path.resolve()

        super().__init__(self._path)

        self.track_app = track_app

        self._file_states = _get_file_states(self._path)
        if track_app.ignore_temporaries:
            self._file_states = {
                path: file_state
                for path, file_state in self._file_states.items()
                if not (
                    file_state == _FileState.UNTRACKED
                    and re.search(
                        r"((^|/)\.)|((^|/)__pycache__|venv(/|$))|(\.(bak|o|pyc|so|tar(\.gz|\.zst)?|whl)(/|$))",
                        str(path.relative_to(self._path)),
                    )
                    is not None
                )
            }

        self._untracked_paths = None
        self._expand_paths = None

        self._update_paths()

    def filter_paths(self, paths):
        filtered_paths = []
        for path in paths:
            if path.is_symlink() or path.is_file():
                if self._file_states.get(path) in (None, _FileState.UNCHANGED):
                    continue

            else:
                _v_ = path not in typing.cast(set, self._untracked_paths)
                if _v_ and path not in typing.cast(set, self._expand_paths):
                    continue

            filtered_paths.append(path)

        return filtered_paths

    def render_label(self, node, base_style, style):
        if node.allow_expand:
            return super().render_label(node, base_style, style)

        _v_ = typing.cast(tw__directory_tree.DirEntry, node.data).path
        color_name = _DirectoryTree._COLOR_NAMES.get(self._file_states.get(_v_))

        if color_name is not None:
            _v_ = [r_style.Style(color=color_name, bgcolor=style.bgcolor)]
            style = style.combine(_v_)

        return super().render_label(node, base_style, style)

    def on_tree_node_expanded(self, node_expanded):
        for node in node_expanded.node.children:
            if node.data.path in self._expand_paths:
                self._reload_node(node)

    def action_select_cursor(self):
        cursor_node = typing.cast(tw__tree.TreeNode, self.cursor_node)
        cursor_path = typing.cast(tw__directory_tree.DirEntry, cursor_node.data).path

        if cursor_node.allow_expand:  # directory
            path_string = f"{cursor_path}/"
            any = False
            for path, file_state in self._file_states.items():
                _v_ = file_state == _FileState.ADDED
                if _v_ and str(path).startswith(path_string):  # remove
                    self._file_states[path] = _FileState.UNTRACKED
                    any = True

            if not any:  # add
                for path, file_state in self._file_states.items():
                    _v_ = file_state == _FileState.UNTRACKED
                    if _v_ and str(path).startswith(path_string):
                        self._file_states[path] = _FileState.ADDED

            self._update_paths()
            self._reload_node(cursor_node)

        else:
            file_state = self._file_states.get(cursor_path)
            if file_state == _FileState.UNTRACKED:  # add
                self._file_states[cursor_path] = _FileState.ADDED
                self._update_paths()
                self._invalidate()

            elif file_state == _FileState.ADDED:  # remove
                self._file_states[cursor_path] = _FileState.UNTRACKED
                self._update_paths()
                self._invalidate()

    def action_search(self):
        self.track_app.start_search()

    def search(self, pattern, untrack):
        from_state, to_state = (
            (_FileState.ADDED, _FileState.UNTRACKED)
            if untrack
            else (_FileState.UNTRACKED, _FileState.ADDED)
        )

        for path, file_state in self._file_states.items():
            if file_state != from_state:
                continue

            if pattern.search(str(path.relative_to(self._path))) is None:
                continue

            self._file_states[path] = to_state

        self._update_paths()
        self._reload_node(self.root)

    def _update_paths(self):
        untracked_paths = set()
        expand_paths = set()

        for path, file_state in self._file_states.items():
            if file_state == _FileState.UNTRACKED:
                _set = untracked_paths

            elif file_state in (_FileState.ADDED, _FileState.MODIFIED):
                _set = expand_paths

            else:
                continue

            _path = path.parent
            while True:
                _set.add(_path)

                if _path == self._path:
                    break

                _path = _path.parent

        self._untracked_paths = untracked_paths
        self._expand_paths = expand_paths

    def action_continue(self):
        self.track_app.exit(0)

    def _reload_node(self, node):
        if node == self.root:
            self.reload()

        else:
            self.reload_node(node)


class _SearchInput(t_widgets.Input):
    _v_ = ("escape", "cancel()", "Cancel")
    BINDINGS = [_v_, ("up", "previous()", "Previous"), ("down", "next()", "Next")]

    def __init__(self, search_tuples):
        super().__init__()

        self.search_tuples = search_tuples

        self._index = -1

    def action_previous(self):
        self._index -= 1
        if self._index < 0:
            self._index = len(self.search_tuples) - 1

        self._update_value()

    def action_next(self):
        self._index += 1
        if len(self.search_tuples) <= self._index:
            self._index = 0

        self._update_value()

    def _update_value(self):
        if not (0 <= self._index and self._index < len(self.search_tuples)):
            return

        pattern, untrack = self.search_tuples[self._index]
        prefix_string = "u" if untrack else "t"
        self.value = f"{prefix_string} {pattern.pattern}"
        self.cursor_position = len(self.value)

    def on_input_submitted(self):
        track_app = typing.cast(_TrackApp, self.app)

        match = re.search(r"^\s*(?P<prefix>[+t\-u]) (?P<pattern>.*)", self.value)
        if match is None:
            track_app.show_info(
                "Start pattern with either\n\n    + or t  to track\nor  - or u  to"
                " untrack files\n\nfollowed by a space.\nExample: `t your pattern`"
                " (without ``)"
            )
            return

        prefix_string, pattern_string = match.group("prefix", "pattern")

        pattern_string = pattern_string.strip()
        if pattern_string == "":
            return

        try:
            pattern = re.compile(pattern_string)

        except Exception as exception:
            track_app.show_error(str(exception))

        else:
            track_app.end_search(pattern, prefix_string in ("-", "u"))

    def action_cancel(self):
        typing.cast(_TrackApp, self.app).end_search(None, None)


class _MessageScreen(t_screen.ModalScreen):
    BINDINGS = [("enter", "exit()", "Ok"), ("escape", "exit()", "Ok")]

    def __init__(self, string, is_error):
        super().__init__()

        self.string = string
        self.is_error = is_error

    def compose(self):
        label = t_widgets.Label(self.string)
        if self.is_error:
            label.add_class("error")
        yield label

    def action_exit(self):
        self.app.pop_screen()


def _get_file_states(path):
    file_states = {}

    process = ["git", "status", "--porcelain"] >> subprocess_shell.start(cwd=path)

    for line_string in process.get_stdout_lines():
        _v_ = re.search(r"^(?P<state>[ A-Z?]{2})\s+(?P<path>.*)$", line_string)
        state_string, path_string = typing.cast(re.Match, _v_).group("state", "path")

        if "?" in state_string:
            file_state = _FileState.UNTRACKED

        elif "A" in state_string:
            file_state = _FileState.ADDED

        elif "D" in state_string:
            file_state = _FileState.DELETED

        else:
            file_state = _FileState.MODIFIED

        path_string = path_string.strip()

        _v_ = path_string.endswith("/")
        _v_ = _iter_file_paths(path / path_string) if _v_ else [path / path_string]
        file_states.update((path, file_state) for path in _v_)

    _ = process >> subprocess_shell.wait()
    return file_states


def _iter_file_paths(path):
    if path.is_symlink() or path.is_file():
        yield path

    elif path.is_dir():
        for path in path.iterdir():
            yield from _iter_file_paths(path)


if __name__ == "__main__":
    typer.run(main)
