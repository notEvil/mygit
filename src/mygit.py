#!/usr/bin/env python

import git
import git.repo as g_repo
import subprocess_shell
import typer
import typing_extensions
import datetime
import enum
import pathlib
import re
import socket
import subprocess
import sys
import typing
import urllib.parse as u_parse


PATH = pathlib.Path(__file__).parent

_ERROR_STYLE = "red"
_INFO_STYLE = "bright_black"
_SUCCESS_STYLE = "green"
_WARNING_STYLE = "dark_orange"


try:
    import rich.console as r_console

except ModuleNotFoundError:

    def _(print):
        def _print(*args, style=None, **kwargs):
            return print(*args, **kwargs)

        return _print

    print = _(print)

else:
    stdout_console = r_console.Console(highlight=False, file=sys.stdout)
    stderr_console = r_console.Console(highlight=False, file=sys.stderr)

    def _(print):
        def _print(*args, file=sys.stdout, flush=None, style=None, **kwargs):
            if file is sys.stdout:
                console = stdout_console

            elif file is sys.stderr:
                console = stderr_console

            else:
                return print(*args, file=file, flush=flush, **kwargs)

            return console.out(*args, style=style, **kwargs)

        return _print

    print = _(print)


def print_stderr(*args, **kwargs):
    return print(*args, file=sys.stderr, **kwargs)


_typer = typer.Typer()


def _switch(
    branch: typing_extensions.Annotated[
        typing.Optional[str], typer.Argument(show_default=False)
    ] = None,
    detach: typing_extensions.Annotated[
        bool, typer.Option("--detach", "-d", help="Detach HEAD")
    ] = False,
):
    """
    Switch to another branch
    """

    process = ["git", "branch", "--show-current"] >> subprocess_shell.start()

    _v_ = typing.cast(str, process >> subprocess_shell.read(return_codes=(0, 128)))
    current_branch_name = _v_.strip()

    if process.get_subprocess().wait() == 128:
        sys.exit(2)

    if branch is None:
        branch = current_branch_name

    _v_ = ["git", "status", "--porcelain", "--untracked-files=no"]
    _v_ = typing.cast(str, _v_ >> subprocess_shell.start() >> subprocess_shell.read())
    if len(_v_) != 0:
        if current_branch_name == branch:
            return

        lines_strings = [
            line_string
            for _, stash_branch_name, line_string in _get_stashes()
            if stash_branch_name == current_branch_name
        ]

        if len(lines_strings) != 0:
            _v_ = "There are stashed changes for the current branch:"
            print_stderr(_v_, style=_ERROR_STYLE)

            for line_string in lines_strings:
                print_stderr(f"  {line_string}", end="")

            sys.exit(3)

        print_stderr("! stash", style=_WARNING_STYLE)

        _v_ = _process(["git", "stash", "--message", "[mygit switch]"])
        _ = _v_ >> subprocess_shell.wait()

    print_stderr("! switch", style=_WARNING_STYLE)

    _v_ = filter(None, ["git", "switch", "--detach" if detach else None, branch])
    return_code = _process(_v_) >> subprocess_shell.wait(return_codes=(0, 128))

    if return_code == 128:
        sys.exit(4)

    for index, stash_branch_name, _ in _get_stashes():
        if stash_branch_name == branch:
            print_stderr("! unstash", style=_WARNING_STYLE)

            _v_ = _process(["git", "stash", "pop", "--index", f"stash@{{{index}}}"])
            _ = _v_ >> subprocess_shell.wait()

            break


def _get_stashes():
    process = ["git", "stash", "list"] >> subprocess_shell.start()

    for index, line_string in enumerate(process.get_stdout_lines()):
        _v_ = r"^stash@\{(?P<index>\d+)\}: (?P<on>[^:]+): (?P<message>.+)$"
        match = typing.cast(re.Match, re.search(_v_, line_string))

        assert int(match.group("index")) == index

        on, message = match.group("on", "message")
        if message != "[mygit switch]":
            continue

        match = re.search(r"^On (?P<branch>\S+)$", on)
        if match is None:
            continue

        yield (index, match.group("branch"), line_string)

    _ = process >> subprocess_shell.wait()


_typer.command("switch")(_switch)
_typer.command("sw", help="Alias for switch")(_switch)


class _StashOptions(enum.StrEnum):
    local = enum.auto()
    remote = enum.auto()


@_typer.command()
def sync(
    path: typing_extensions.Annotated[
        typing.Optional[pathlib.Path],
        typer.Argument(
            show_default=False, help="Path used to initialize link to remote repository"
        ),
    ] = None,
    stash: typing_extensions.Annotated[
        typing.Optional[_StashOptions],
        typer.Option(show_default=False, help="Which side to stash if diverged"),
    ] = None,
    backups: typing_extensions.Annotated[
        int, typer.Option(help="Maximum number of backups")
    ] = 3,
    find_commit: typing_extensions.Annotated[
        bool,
        typer.Option(help="Use `mygit find-commits --exact` to identify false dirt"),
    ] = True,
    show: typing_extensions.Annotated[
        bool, typer.Option(help="Just show, don't execute")
    ] = False,
):
    """
    Push/pull changes to/from linked directory
    """

    if path is None:
        local_path = _get_repo_path(None, 3)
        path = (local_path / ".git").readlink().parent

    else:
        if not (path / ".git").exists():
            print_stderr("Target is not a Git repository", style=_ERROR_STYLE)
            sys.exit(3)

        local_path = pathlib.Path(".")

        print_stderr("! create link", style=_WARNING_STYLE)
        (local_path / ".git").symlink_to(path / ".git")

        _backup(local_path, socket.gethostname(), backups)

        print_stderr("! restore local", style=_WARNING_STYLE)

        _v_ = _process(["git", "restore", "--source", "HEAD", "."], cwd=local_path)
        _ = _v_ >> subprocess_shell.wait()

    local_is_dirty = _is_dirty(local_path)
    if local_is_dirty and find_commit:
        print_stderr("finding local commit ..", end="", flush=True, style=_INFO_STYLE)
        commits = find_commits(path=local_path, exact=True)
        print_stderr()
        if len(commits) != 0:
            print_stderr(f"found {next(iter(commits))}")
            print_stderr("! restore local", style=_WARNING_STYLE)

            if not show:
                _v_ = ["git", "restore", "--source", "HEAD", "."]
                _ = _process(_v_, cwd=local_path) >> subprocess_shell.wait()

            local_is_dirty = False

    remote_is_dirty = _is_dirty(path)
    if remote_is_dirty and find_commit:
        print_stderr("finding remote commit ..", end="", flush=True, style=_INFO_STYLE)
        commits = find_commits(path=path, exact=True)
        print_stderr()
        if len(commits) != 0:
            print_stderr(f"found {next(iter(commits))}")
            print_stderr("! restore remote", style=_WARNING_STYLE)

            if not show:
                _v_ = _process(["git", "restore", "--source", "HEAD", "."], cwd=path)
                _ = _v_ >> subprocess_shell.wait()

            remote_is_dirty = False

    if show:
        print(
            ("diverged" if remote_is_dirty else "push")
            if local_is_dirty
            else ("pull" if remote_is_dirty else "in sync")
        )
        return

    if local_is_dirty and remote_is_dirty:
        match stash:
            case _StashOptions.local:
                print_stderr("! stash", style=_WARNING_STYLE)

                _v_ = _get_stash_message(datetime.datetime.now(), socket.gethostname())
                _v_ = _process(["git", "stash", "--message", _v_], cwd=local_path)
                _ = _v_ >> subprocess_shell.wait()

                local_is_dirty = False

            case _StashOptions.remote:
                print_stderr("! stash", style=_WARNING_STYLE)

                _v_ = _get_stash_message(datetime.datetime.now(), "remote")
                _v_ = _process(["git", "stash", "--message", _v_], cwd=path)
                _ = _v_ >> subprocess_shell.wait()

                remote_is_dirty = False

            case None:
                _v_ = "Both directories contain changes and argument `stash` is not set"
                print_stderr(_v_, style=_ERROR_STYLE)

                sys.exit(2)

    if local_is_dirty:
        _sync(local_path, path, socket.gethostname(), backups)
        print_stderr("Successfully pushed to `", end="", style=_SUCCESS_STYLE)
        print_stderr(path, end="")
        print_stderr("`", style=_SUCCESS_STYLE)

    elif remote_is_dirty:
        _sync(path, local_path, "remote", backups)
        print_stderr("Successfully pulled from `", end="", style=_SUCCESS_STYLE)
        print_stderr(path, end="")
        print_stderr("`", style=_SUCCESS_STYLE)

    else:
        print_stderr("Nothing to sync", style=_SUCCESS_STYLE)


def _is_dirty(path):
    _v_ = ["git", "status", "--porcelain", "--untracked-files=no"]
    _v_ = _v_ >> subprocess_shell.start(cwd=path) >> subprocess_shell.read()
    if len(typing.cast(str, _v_)) == 0:
        return False

    _v_ = ["git", "diff", "--quiet", "HEAD"] >> subprocess_shell.start(cwd=path)
    return _v_ >> subprocess_shell.wait(return_codes=(0, 1)) == 1


def _sync(source_path, target_path, side_string, backups):
    print_stderr("! apply", style=_WARNING_STYLE)

    diff_arguments = ["git", "diff", "HEAD", "--"]
    apply_arguments = ["git", "apply"]

    source_string = "" if source_path == pathlib.Path(".") else f"@ {source_path}  "
    target_string = "" if target_path == pathlib.Path(".") else f"  @ {target_path}"
    print_stderr(
        f"> {source_string}{subprocess.list2cmdline(diff_arguments)} |"
        f" {subprocess.list2cmdline(apply_arguments)}{target_string}"
    )

    _v_ = subprocess_shell.start(cwd=source_path, pass_stdout=True) + apply_arguments
    _v_ = diff_arguments >> _v_ >> subprocess_shell.start(cwd=target_path)
    _ = _v_ >> subprocess_shell.wait()

    _backup(source_path, side_string, backups)

    print_stderr("! restore", style=_WARNING_STYLE)

    _v_ = _process(["git", "restore", "--source=HEAD", "."], cwd=source_path)
    _ = _v_ >> subprocess_shell.wait()


def _backup(path, side_string, backups):
    now_datetime = datetime.datetime.now()

    process = ["git", "stash", "list"] >> subprocess_shell.start(cwd=path)
    indices = []

    for index, line_string in enumerate(process.get_stdout_lines()):
        assert line_string.startswith(f"stash@{{{index}}}: ")

        _v_ = r"\[mygit sync \d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2} [^]]+\]"
        if re.search(_v_, line_string) is not None:
            indices.append(index)

    _ = process >> subprocess_shell.wait()

    for index in reversed(indices[max(0, backups - 1) :]):
        print_stderr("! remove backup", style=_WARNING_STYLE)

        _v_ = _process(["git", "stash", "drop", f"stash@{{{index}}}"], cwd=path)
        _ = _v_ >> subprocess_shell.wait()

    if backups == 0:
        return

    print_stderr("! create backup", style=_WARNING_STYLE)

    _v_ = _get_stash_message(now_datetime, side_string)
    _v_ = _process(["git", "stash", "--keep-index", "--message", _v_], cwd=path)
    _ = _v_ >> subprocess_shell.wait()


def _get_stash_message(datetime_, side_string):
    datetime_string = datetime_.strftime("%Y-%m-%dT%H:%M:%S")
    return f"[mygit sync {datetime_string} {side_string}]"


@_typer.command("find-commits")
def _find_commits(
    scores: typing_extensions.Annotated[bool, typer.Option(help="With scores")] = False,
    exact: typing_extensions.Annotated[
        bool, typer.Option(help="Only exact matches")
    ] = False,
    referenced: typing_extensions.Annotated[
        bool, typer.Option(help="Only referenced commits")
    ] = False,
    include_stash: typing_extensions.Annotated[
        bool, typer.Option(help="Consider stash")
    ] = False,
    head_shortcut: typing_extensions.Annotated[
        bool, typer.Option(help="If HEAD matches exactly, stop immediatly")
    ] = True,
    deleted_shortcut: typing_extensions.Annotated[
        bool,
        typer.Option(
            help="If any file doesn't exist in working directory, ignore commit"
        ),
    ] = True,
    subset_shortcut: typing_extensions.Annotated[
        bool,
        typer.Option(
            help=(
                "If there is another commit with a larger set of exactly matching"
                " files, ignore commit"
            )
        ),
    ] = True,
):
    """
    Find commits similar to the working directory
    """

    _scores = find_commits(
        exact=exact,
        referenced=referenced,
        include_stash=include_stash,
        head_shortcut=head_shortcut,
        deleted_shortcut=deleted_shortcut,
        subset_shortcut=subset_shortcut,
    )
    if len(_scores) == 0:
        sys.exit(2)

    for object, score in sorted(_scores.items(), key=lambda tuple: tuple[1]):
        print(object, end="")
        if scores:
            print(f" {score}", end="")
        print()


def find_commits(
    path=None,
    exact=False,
    referenced=False,
    include_stash=False,
    head_shortcut=True,
    deleted_shortcut=True,
    subset_shortcut=True,
):
    path = _get_repo_path(path, 2)
    repo = g_repo.Repo(path)

    # compare working tree with HEAD
    _v_ = {diff.a_path for diff in repo.head.commit.diff(None) if not diff.new_file}
    dirty_path_strings = _v_

    if head_shortcut and len(dirty_path_strings) == 0:
        return {repo.head.commit: 0}
    #

    if referenced:
        commits = {
            reference.commit
            for reference in repo.references
            if not (not include_stash and reference.name == "refs/stash")
        }

    else:
        _v_ = dict(all=True) if include_stash else dict(exclude="refs/stash", all=True)
        commits = list(repo.iter_commits(**_v_))

    # get hashes
    hashes = {
        object.path: object.binsha
        for object in repo.head.commit.tree.traverse()
        if isinstance(object, git.Blob) and object.path not in dirty_path_strings
    }

    path_strings = [
        path_string
        for path_string in {
            object.path
            for commit in commits
            for object in commit.tree.traverse()
            if isinstance(object, git.Blob)
        }
        if path_string not in hashes and (path / path_string).exists()
    ]

    if len(path_strings) != 0:
        process = ["git", "hash-object", "--stdin-paths"] >> subprocess_shell.start()
        for path_string in path_strings:
            _ = process >> subprocess_shell.write(f"{path / path_string}\n")

        _v_ = typing.cast(str, process >> subprocess_shell.read()).strip().split("\n")
        _v_ = zip(path_strings, (int(string, 16).to_bytes(20) for string in _v_))
        hashes.update(_v_)
    #

    scores = {}

    # filter commits
    commit_lists = {}

    for commit in commits:
        path_strings = set()

        for object in commit.tree.traverse():
            if not isinstance(object, git.Blob):
                continue

            hash = hashes.get(object.path)
            if deleted_shortcut and hash is None:  # ignore commit if a file is deleted
                break

            if exact:
                if object.binsha != hash:
                    break

                continue

            if object.binsha == hash:
                path_strings.add(object.path)

        else:
            if exact:
                scores[commit] = 0
                continue

            if subset_shortcut:
                # ignore commits for which there is another commit with a superset of exact matching files
                path_strings = frozenset(path_strings)
                add = False

                for existing_path_strings, commit_list in list(commit_lists.items()):
                    if not add and path_strings.issubset(existing_path_strings):
                        if len(path_strings) == len(existing_path_strings):
                            commit_list.append(commit)

                        break

                    if path_strings.issuperset(existing_path_strings):
                        commit_lists.pop(existing_path_strings)
                        add = True

                else:
                    commit_lists[path_strings] = [commit]

            else:
                commit_lists.setdefault(frozenset(), []).append(commit)
    #

    scores.update(
        (
            commit,
            sum(
                diff.diff.count(b"\n") for diff in commit.diff(None, create_patch=True)
            ),
        )
        for commit_list in commit_lists.values()
        for commit in commit_list
    )
    return scores


@_typer.command()
def reset(
    source: typing_extensions.Annotated[str, typer.Argument(show_default=False)],
    branch: typing_extensions.Annotated[
        bool, typer.Option(help="Reset branch HEAD")
    ] = False,
    index: typing_extensions.Annotated[bool, typer.Option(help="Reset index")] = False,
    working: typing_extensions.Annotated[
        bool, typer.Option(help="Reset working directory")
    ] = False,
):
    """
    Reset HEAD or any combination of branch head, index and working directory
    """

    repo = g_repo.Repo(_get_repo_path(None, 2))
    index_diff_bytes = None
    commit = None

    if working:
        assert source is not None

        if not index:
            _v_ = ["git", "diff", "--cached", source, "--"] >> subprocess_shell.start()
            index_diff_bytes = _v_ >> subprocess_shell.read(bytes=True)

        if not branch:
            commit = repo.head.commit

        _v_ = "! reset branch, index and working tree to `"
        print_stderr(_v_, end="", style=_WARNING_STYLE)

        print_stderr(source, end="")
        print_stderr("`", style=_WARNING_STYLE)

        repo.head.reset(source, index=True, working_tree=True)

    elif index:
        assert source is not None

        if not branch:
            commit = repo.head.commit

        print_stderr("! reset branch and index to `", end="", style=_WARNING_STYLE)
        print_stderr(source, end="")
        print_stderr("`", style=_WARNING_STYLE)
        repo.head.reset(source, index=True, working_tree=False)

    elif branch:
        assert source is not None
        print_stderr("! reset branch to `", end="", style=_WARNING_STYLE)
        print_stderr(source, end="")
        print_stderr("`", style=_WARNING_STYLE)
        repo.head.reset(source, index=False, working_tree=False)

    else:
        for reference in repo.references:
            if reference.name == source:
                repo.head.set_reference(reference)
                break

        else:
            repo.head.set_reference(source)

        return

    if index_diff_bytes is not None and index_diff_bytes != b"":
        print_stderr("! apply", style=_WARNING_STYLE)

        _v_ = subprocess_shell.write(typing.cast(bytes, index_diff_bytes))
        _ = _process(["git", "apply", "--cached"]) >> _v_ >> subprocess_shell.wait()

    if commit is not None:
        print_stderr("! reset branch to `", end="", style=_WARNING_STYLE)
        print_stderr(commit.hexsha, end="")
        print_stderr("`", style=_WARNING_STYLE)
        repo.head.reset(commit, index=False, working_tree=False)


# workaround typer
class _Int(int):
    def __new__(cls, object):
        if isinstance(object, int):
            return object

        return super().__new__(cls, object)


_Int.__name__ = "integer"


@_typer.command()
def fetch(
    url: typing_extensions.Annotated[
        typing.Optional[str],
        typer.Argument(show_default=False, help="URL used to clone repository"),
    ] = None,
    branch: typing_extensions.Annotated[
        typing.Optional[str], typer.Option("--branch", "-b", show_default=False)
    ] = None,
    number: typing_extensions.Annotated[
        int,
        typer.Option("--number", "-n", parser=_Int, help="Number of commits to fetch"),
    ] = 10,
    tags: typing_extensions.Annotated[bool, typer.Option(help="Fetch tags")] = False,
    remote: typing.Optional[str] = None,
):
    """
    Fetch commits
    """

    path = pathlib.Path(".")

    if url is not None:
        # clone
        arguments: list[typing.Any] = ["git", "clone"]
        if branch is not None:
            arguments.extend(("--branch", branch))
        if 0 < number:
            arguments.extend(("--depth", number))
        arguments.append(url)

        _ = _process(arguments, stdout=None, stderr=None) >> subprocess_shell.wait()
        #

        parse_result = u_parse.urlparse(url)

        _v_ = parse_result.path[parse_result.path.rfind("/") + 1 :].removesuffix(".git")
        path = path / _v_

        # reset remote.origin.fetch
        _v_ = "+refs/heads/*:refs/remotes/origin/*"
        _v_ = _process(["git", "config", "remote.origin.fetch", _v_], cwd=path)
        _ = _v_ >> subprocess_shell.wait()

    elif not (branch is None and not isinstance(number, _Int) and tags):
        repo = g_repo.Repo(_get_repo_path(path, 2))

        if remote is None:
            # get local reference
            if branch is None:
                local_reference = _get_head_reference(repo, 3)

            else:
                for local_reference in repo.references:
                    if local_reference.name == branch:
                        break

                else:
                    local_reference = None

            # get remote reference
            if local_reference is None:
                print_stderr(
                    f"Using remote of current branch because branch `{branch}` doesn't"
                    " exist"
                )
                _get_head_reference(repo, 3)

            _v_ = repo.head.reference if local_reference is None else local_reference
            remote_reference = _get_tracking_reference(_v_, 4)

            depth_argument = (
                "--depth=999999"
                if number < 0
                else (
                    f"--depth={max(1, number)}"
                    if local_reference is None
                    else f"--deepen={number}"
                )
            )

            remote = remote_reference.remote_name
            if branch is None:
                branch = remote_reference.remote_head

        else:
            _v_ = "--depth=999999" if number < 0 else f"--depth={max(1, number)}"
            depth_argument = _v_

        _v_ = filter(None, ["git", "fetch", depth_argument, remote, branch])
        _ = _process(_v_, stdout=None, stderr=None) >> subprocess_shell.wait()

    if tags:
        if remote is None:
            _v_ = _get_head_reference(g_repo.Repo(_get_repo_path(path, 2)), 3)
            remote = _get_tracking_reference(_v_, 4).remote_name

        _v_ = ["git", "fetch", remote, "refs/tags/*:refs/tags/*"]
        _ = _process(_v_, stdout=None, stderr=None) >> subprocess_shell.wait()


def _get_head_reference(repo, return_code):
    if repo.head.is_detached:
        print_stderr("Currently not on a branch, consider --remote", style=_ERROR_STYLE)
        sys.exit(return_code)

    return repo.head.reference


def _get_tracking_reference(reference, return_code):
    tracking_reference = reference.tracking_branch()
    if tracking_reference is None:
        print_stderr("Branch `", end="", style=_ERROR_STYLE)
        print_stderr(reference.name, end="")
        print_stderr("` doesn't track remote branch", style=_ERROR_STYLE)
        sys.exit(return_code)

    return tracking_reference


@_typer.command()
def clean_stash(
    keep_mysync: typing_extensions.Annotated[
        bool, typer.Option(help="Keep stashed changes added by mygit")
    ] = True
):
    """
    Drop stashed changes with a convenient UI
    """

    process = ["git", "stash", "list"] >> subprocess_shell.start()

    offset = 0
    for index, line_string in enumerate(process.get_stdout_lines()):
        _v_ = re.search(rf"^stash@\{{{index}\}}:\s+(?P<name>.*)$", line_string)
        match = typing.cast(re.Match, _v_)

        name = match.group("name").strip()

        if keep_mysync and re.search(rf"\[mygit\s[^]]*\]", name) is not None:
            continue

        id_string = f"stash@{{{index + offset}}}"

        while True:
            print()
            print(name)
            print()

            _v_ = ["git", "stash", "show", id_string]
            _ = _v_ >> subprocess_shell.start(stdout=None) >> subprocess_shell.wait()

            print()

            while True:
                input_string = input("[k(eep), d(rop), s(how)] ")

                match input_string:
                    case "k":
                        break

                    case "d":
                        print_stderr("! drop stash", style=_WARNING_STYLE)

                        _v_ = _process(["git", "stash", "drop", id_string])
                        _ = _v_ >> subprocess_shell.wait()

                        offset -= 1
                        break

                    case "s":
                        _v_ = ["git", "stash", "show", "-p", id_string]
                        _v_ = _v_ >> subprocess_shell.start(stdout=None)
                        _ = _v_ >> subprocess_shell.wait()

                        break

            if not (input_string == "s"):
                break

    _ = process >> subprocess_shell.wait()


@_typer.command()
def recover_commit(
    full_hash: typing_extensions.Annotated[
        bool, typer.Option(help="Show full commit hashes")
    ] = False
):
    """
    Recover lost commits
    """

    repo = g_repo.Repo(_get_repo_path(None, 2))

    _v_ = set(repo.iter_commits(all=True, reflog=True))
    commits = _v_ - set(repo.iter_commits(all=True))

    for commit in list(commits):
        commits.difference_update(commit.parents)

    commits = list(commits)
    commits.sort(key=lambda commit: commit.committed_datetime)

    id_template_string = f"{{:{len(str(len(commits)))}}}"

    references = {}
    for reference in repo.references:
        references.setdefault(reference.commit, []).append(reference)

    for index, commit in enumerate(commits):
        message_string = str(commit.message).strip()
        newline_index = message_string.find("\n")
        if newline_index != -1:
            message_string = f"{message_string[:newline_index]}.."

        print(
            f" {id_template_string.format(index + 1)}:"
            f" {commit.committed_datetime} [{commit.hexsha if full_hash else commit.hexsha[:7]}]"
            f" {message_string}"
        )

        for _references, count in _get_parent_commits(commit, references):
            _v_ = sorted(_references, key=lambda reference: reference.name)
            references_string = ", ".join(f"`{reference}`" for reference in _v_)

            print(f"    from {references_string} at depth {count}")

    print()
    while True:
        id_string = input("Which? ")

        try:
            id = int(id_string)

        except Exception:
            pass

        else:
            index = id - 1
            if 0 <= index and index < len(commits):
                break

    branch_name = input("Branch name: ")

    print_stderr("! create branch", style=_WARNING_STYLE)

    _v_ = _process(["git", "branch", branch_name, commits[index].hexsha])
    _ = _v_ >> subprocess_shell.wait()


def _get_parent_commits(commit, references):
    for parent_commit in commit.parents:
        _references = references.get(parent_commit)

        if _references is None:
            for _references, count in _get_parent_commits(parent_commit, references):
                yield (_references, count + 1)

        else:
            yield (_references, 1)


@_typer.command()
def free_space():
    """
    Free space by deleting unchanged files in the working directory

    Undo with `mygit occupy-space`.
    """

    repository_path = _get_repo_path(None, 2).resolve()

    paths = set()
    for path in repository_path.iterdir():
        if path.name == ".git":
            continue

        paths.update(_get_file_paths(path))

    _v_ = ["git", "status", "--porcelain", "--untracked-files=all"]
    process = _v_ >> subprocess_shell.start(cwd=repository_path)

    untracked_count = 0
    deleted_paths = []
    changed_count = 0

    for line_string in process.get_stdout_lines():
        path = repository_path / line_string[3:-1]

        if "?" in line_string[:2]:
            if line_string[-2:-1] == "/":
                continue

            untracked_count += 1

        elif "D" in line_string[:2]:
            deleted_paths.append(path)

        else:
            paths.discard(path)
            changed_count += 1

    _ = process >> subprocess_shell.wait()

    while True:
        print()
        print("!! WARNING !!", style=_WARNING_STYLE)
        print(
            f"This will delete all files in `{repository_path}` ({len(paths)} files,"
            f" {sum(path.stat().st_size for path in paths) / 1e6:.1f} MB)"
        )
        print("!! WARNING !!", style=_WARNING_STYLE)
        print(f"""
except in `.git` and changed files ({changed_count})
and will replace deleted files with empty files ({len(deleted_paths)}).

If you want to keep untracked files ({untracked_count}), you have to answer no and add them first!

"""[1:-1])
        match input("Do you really want to continue? [y(es), n(o)] "):
            case "y":
                break

            case "n":
                return

    if len(deleted_paths) != 0:
        print("! replace deleted files with empty files", style=_WARNING_STYLE)
        for path in deleted_paths:
            path.touch()

    if len(paths) != 0:
        print("! delete files", style=_WARNING_STYLE)
        for path in paths:
            path.unlink()


@_typer.command()
def occupy_space():
    """
    Undo `mygit free-space`
    """
    repository_path = _get_repo_path(None, 2).resolve()

    paths = []
    for path in repository_path.iterdir():
        if path.name == ".git":
            continue

        for path in _get_file_paths(path):
            if path.stat().st_size == 0:
                paths.append(path)

    while True:
        print()
        print("!! WARNING !!", style=_WARNING_STYLE)
        print(f"This will restore all files in `{repository_path}`")
        print("!! WARNING !!", style=_WARNING_STYLE)
        print(f"and will delete files which are currently empty ({len(paths)}).")
        print()
        match input("Do you really want to continue? [y(es), n(o)] "):
            case "y":
                break

            case "n":
                return

    print("! restore repository", style=_WARNING_STYLE)

    _v_ = _process(["git", "restore", "."], cwd=repository_path)
    _ = _v_ >> subprocess_shell.wait()

    print("! delete files", style=_WARNING_STYLE)
    for path in paths:
        path.unlink()


def _get_file_paths(path: pathlib.Path):
    if path.is_symlink():
        return

    if path.is_file():
        yield path

    elif path.is_dir() and not (path / ".git").exists():
        for path in path.iterdir():
            yield from _get_file_paths(path)


def _process(arguments, *args, **kwargs):
    arguments = list(arguments)
    cwd_object = kwargs.get("cwd")
    path_string = "" if cwd_object in (None, pathlib.Path(".")) else f"  @ {cwd_object}"
    print_stderr(f"> {subprocess.list2cmdline(map(str, arguments))}{path_string}")
    return arguments >> subprocess_shell.start(*args, **kwargs)


def _get_repo_path(path, return_code):
    if path is None:
        path = pathlib.Path(".")

    _path = path.resolve()

    if (_path / ".git").exists():
        return path

    for parent_path in _path.parents:
        if (parent_path / ".git").exists():
            return parent_path

    print_stderr("Not in a Git repository: ", end="", style=_ERROR_STYLE)
    print_stderr(_path)
    sys.exit(return_code)


def main():
    append_path = str(PATH) not in sys.path
    if append_path:
        sys.path.append(str(PATH))

    try:
        import mygit_stagetool

    except ModuleNotFoundError:
        mygit_stagetool = None

    try:
        import mygit_track

    except ModuleNotFoundError:
        mygit_track = None

    if append_path:
        sys.path.remove(str(PATH))

    if mygit_stagetool is not None:
        _typer.command("stagetool")(mygit_stagetool.main)
        _typer.command("st", help="Alias for stagetool")(mygit_stagetool.main)

    if mygit_track is not None:
        _typer.command("track")(mygit_track.main)

    _typer()


if __name__ == "__main__":
    main()
