#!/usr/bin/env python

import subprocess_shell
import typer
import typing_extensions
import contextlib
import itertools
import os
import os.path as o_path
import pathlib
import re
import shlex
import shutil
import sys
import tempfile


PATH = pathlib.Path(__file__).parent


import __main__

if __main__.__file__.endswith("/mygit.py"):
    mygit = __main__

else:
    append_path = str(PATH) not in sys.path
    if append_path:
        sys.path.append(str(PATH))

    import mygit

    if append_path:
        sys.path.remove(str(PATH))


def main(
    staged: typing_extensions.Annotated[
        bool, typer.Option("--staged/--no-staged", "--cached/--no-cached")
    ] = False,
    working: typing_extensions.Annotated[
        bool, typer.Option(help="Like --staged")
    ] = False,
    two_way: typing_extensions.Annotated[
        bool,
        typer.Option(
            help='Without "current index" (i). Requires either --staged or --working'
        ),
    ] = False,
    symlinks: bool = True,
    update_working: typing_extensions.Annotated[
        bool,
        typer.Option(
            help="Explicitly update working tree. Preferred over just --symlinks"
        ),
    ] = True,
    command: typing_extensions.Annotated[
        str, typer.Option(help="Command format string")
    ] = "meld",
    dir_diff: typing_extensions.Annotated[
        bool, typer.Option("--dir-diff/--no-dir-diff", "-d")
    ] = True,
    reverse_order: typing_extensions.Annotated[
        bool,
        typer.Option(
            help=(
                'Show directories in reverse order, e.g. "working" | "index" | "commit"'
            )
        ),
    ] = False,
    track: typing_extensions.Annotated[
        bool, typer.Option(help="Run `mygit track` in preparation")
    ] = True,
):
    """
    With this Python script you can edit the index (staged changes) and working tree of a Git repository using your favorite diff tool.

    By default, it creates the directories

    - "working": w

    -   "index": _

    -  "commit": c

    in a temporary directory and starts Meld with "commit" to the left, "index" in the middle and "working" to the right.
    When Meld is closed, it validates the changes and waits for confirmation to apply.

    With --staged/--cached: shows the directories "commit", "index" and "current index" (i)

    With --working: shows the directories "current index" (i), "index" and "working"

    With --two-way: doesn't show the directory "current index" (i)

    With --no-symlinks: copies files to "working"

    With --update-working (default): symlinks files to "current working" (W) instead, copies files to "working" and eventually validates and applies changes to the working tree

    With --command: uses `command.format("/path/to/left", "/path/to/middle", "/path/to/right")`.
    If the command doesn't change, appends the paths.

    With --reverse_order: shows the directories in reverse order
    """

    if staged and working:
        _v_ = "Arguments --staged/--cached and --working are mutually exclusive"
        mygit.print_stderr(_v_, style=mygit._ERROR_STYLE)

        sys.exit(2)

    if two_way and not staged and not working:
        mygit.print_stderr(
            "Argument --two-way requires at least one of --staged/cached and --working",
            style=mygit._ERROR_STYLE,
        )
        sys.exit(3)

    if track:
        append_path = str(PATH) not in sys.path
        if append_path:
            sys.path.append(str(PATH))

        try:
            import mygit_track

        except ModuleNotFoundError:
            mygit_track = None

            _v_ = "mygit_track.py or one of its dependencies is not available"
            mygit.print_stderr(_v_, style=mygit._WARNING_STYLE)

        if append_path:
            sys.path.remove(str(PATH))

        if mygit_track is not None:
            if mygit_track.main(_code_offset=128) is False:
                return

    repository_path = mygit._get_repo_path(None, 4)

    with tempfile.TemporaryDirectory() as path_string:
        temp_path = pathlib.Path(path_string)

        index_patch_path = temp_path / "index.patch"
        commit_patch_path = temp_path / "commit.patch"

        # get index patch
        _v_ = ["diff", "--binary", "--exit-code"]
        _v_ = _git(_v_, repository_path, stdout=index_patch_path)
        working_changes = _v_ >> subprocess_shell.wait(return_codes=(0, 1)) == 1

        if working and not working_changes:
            return

        # get commit patch
        if not working:
            _v_ = ["diff", "--binary", "--exit-code", "--staged"]
            _v_ = _git(_v_, repository_path, stdout=commit_patch_path)
            commit_changes = _v_ >> subprocess_shell.wait(return_codes=(0, 1)) == 1

            _v_ = staged and not commit_changes
            if _v_ or (
                not working
                and not staged
                and not working_changes
                and not commit_changes
            ):
                return

        else:
            commit_patch_path.touch()

        # get paths
        relative_paths = set()
        renamed_paths = []

        with (
            open(index_patch_path, "rb") as index_patch_file,
            open(commit_patch_path, "rb") as commit_patch_file,
        ):
            for line_bytes in itertools.chain(index_patch_file, commit_patch_file):
                line_string = line_bytes.decode()

                if line_string.startswith("diff --git "):
                    string = line_string[len("diff --git ") : -1]

                    _v_ = lambda match: abs(
                        match.start() + 1 + len(match.group("quote")) - len(string) // 2
                    )
                    match = min(
                        re.finditer(r'[^ /](?P<quote>"?) (?P=quote)b/', string), key=_v_
                    )

                    if match.group("quote") == "":
                        start_index = 2
                        end_index = None

                    else:
                        start_index = 3
                        end_index = -1

                    first_path = pathlib.Path(string[start_index : match.start() + 1])
                    second_path = pathlib.Path(string[match.end() : end_index])
                    relative_paths.add(second_path)

                    if first_path != second_path:
                        renamed_paths.append((first_path, second_path))
        #

        working_path = temp_path / "w"
        index_path = temp_path / "i"
        commit_path = temp_path / "c"
        edit_path = temp_path / "_"

        # create working
        for relative_path in relative_paths:
            from_path = repository_path / relative_path
            to_path = working_path / relative_path
            to_path.parent.mkdir(parents=True, exist_ok=True)

            if from_path.exists():
                (os.symlink if symlinks else shutil.copy)(from_path.resolve(), to_path)

        # create index
        shutil.copytree(working_path, index_path)

        _v_ = ["git", "apply", "-p1", "-R", "--allow-empty", index_patch_path]
        _ = _v_ >> subprocess_shell.start(cwd=index_path) >> subprocess_shell.wait()

        # create commit
        if not working:
            shutil.copytree(index_path, commit_path)

            _v_ = ["git", "apply", "-p1", "-R", "--allow-empty", str(commit_patch_path)]
            _v_ = _v_ >> subprocess_shell.start(cwd=commit_path)
            _ = _v_ >> subprocess_shell.wait()

            for first_path, second_path in renamed_paths:
                (commit_path / second_path).parent.mkdir(parents=True, exist_ok=True)

                _v_ = pathlib.Path(o_path.relpath(first_path, start=second_path.parent))
                (commit_path / second_path).symlink_to(_v_)

        # create missing files
        for relative_path in relative_paths:
            path = working_path / relative_path
            if not path.exists():
                path.parent.mkdir(parents=True, exist_ok=True)
                path.touch()

            path = index_path / relative_path
            if not path.exists():
                path.parent.mkdir(parents=True, exist_ok=True)
                path.touch()
        #

        shutil.copytree(index_path, edit_path)

        update_working_path = None
        working_diff_bytes = None

        if update_working:
            update_working_path = temp_path / "W"
            working_path.rename(update_working_path)
            shutil.copytree(update_working_path, working_path)

        if two_way:
            if working:
                directory_paths = [edit_path, working_path]
            elif staged:
                directory_paths = [commit_path, edit_path]
            else:
                raise Exception
        else:
            if working:
                directory_paths = [index_path, edit_path, working_path]
            elif staged:
                directory_paths = [commit_path, edit_path, index_path]
            else:
                directory_paths = [commit_path, edit_path, working_path]

        if reverse_order:
            directory_paths.reverse()

        while True:
            if dir_diff:
                command_string = command.format(*directory_paths)
                arguments = shlex.split(command_string)
                if command_string == command:
                    arguments.extend(map(str, directory_paths))

                _v_ = arguments >> subprocess_shell.start(cwd=temp_path)
                _ = _v_ >> subprocess_shell.wait()

            else:
                yes = False

                for relative_path in sorted(relative_paths):
                    if not yes:
                        sys.stderr.flush()

                        while True:
                            _v_ = f"[Y(ES), y(es), n(o), N(O)] {relative_path} ? "
                            input_string = input(_v_)

                            if input_string in ["Y", "y", "n", "N"]:
                                break

                        match input_string:
                            case "Y":
                                yes = True

                            case "n":
                                continue

                            case "N":
                                break

                    file_paths = [path / relative_path for path in directory_paths]
                    command_string = command.format(*file_paths)
                    arguments = shlex.split(command_string)
                    if command_string == command:
                        arguments.extend(map(str, file_paths))

                    _v_ = arguments >> subprocess_shell.start(cwd=temp_path)
                    _ = _v_ >> subprocess_shell.wait()

            # get index patch
            with _out_empty_files([index_path, edit_path]):
                _v_ = _get_patch(pathlib.Path("i"), pathlib.Path("_"), temp_path)
                index_diff_bytes = _v_
            #

            applies = True

            # check index patch
            if index_diff_bytes != b"":
                _v_ = ["--cached", "--check"]
                _v_ = _git_apply(index_diff_bytes, _v_, repository_path)
                if _v_ >> subprocess_shell.wait(return_codes=None) != 0:
                    applies = False
            #

            if applies and update_working:
                # get working patch
                with _out_empty_files([update_working_path, working_path]):
                    _v_ = _get_patch(pathlib.Path("W"), pathlib.Path("w"), temp_path)
                    working_diff_bytes = _v_

                # check working patch
                if working_diff_bytes != b"":
                    _v_ = _git_apply(working_diff_bytes, ["--check"], repository_path)
                    if _v_ >> subprocess_shell.wait(return_codes=None) != 0:
                        applies = False

            if applies:
                break

            sys.stderr.flush()
            print()

            while True:
                input_string = input("Retry [y(es), n(o)]? ")

                if input_string in ["y", "n"]:
                    break

            if input_string == "n":
                return

            print()

    if index_diff_bytes == b"" and not (update_working and working_diff_bytes != b""):
        return

    if index_diff_bytes != b"":
        print()

        _v_ = _git_apply(index_diff_bytes, ["--cached", "--stat"], repository_path)
        _ = _color_stat(_v_) >> subprocess_shell.wait()

    if update_working and working_diff_bytes != b"":
        print()
        print("Working")
        print()

        _v_ = _color_stat(_git_apply(working_diff_bytes, ["--stat"], repository_path))
        _ = _v_ >> subprocess_shell.wait()

    sys.stderr.flush()
    print()

    while True:
        input_string = input("Apply [y(es), n(o)]? ")

        if input_string in ["y", "n"]:
            break

    if input_string == "y":
        if index_diff_bytes != b"":
            mygit.print_stderr("! apply to index", style=mygit._WARNING_STYLE)

            _v_ = ["--cached"]
            _v_ = _git_apply(index_diff_bytes, _v_, repository_path, modifying=True)
            _ = _v_ >> subprocess_shell.wait()

        if working_diff_bytes != b"":
            _v_ = "! apply to working directory"
            mygit.print_stderr(_v_, style=mygit._WARNING_STYLE)

            _v_ = _git_apply(working_diff_bytes, [], repository_path, modifying=True)
            _ = _v_ >> subprocess_shell.wait()


@contextlib.contextmanager
def _out_empty_files(directory_paths):
    unlinked_paths = []

    try:
        for directory_path in directory_paths:
            for file_path in _get_empty_files(directory_path):
                file_path.unlink()
                unlinked_paths.append(file_path)

        yield

    finally:
        for file_path in unlinked_paths:
            file_path.touch()


def _get_empty_files(directory_path):
    for path in directory_path.iterdir():
        if path.is_file():
            if path.stat(follow_symlinks=False).st_size == 0:
                yield path

        elif path.is_dir():
            yield from _get_empty_files(path)


def _get_patch(first_path, second_path, base_path):
    # git diff doesn't follow symlinks
    _v_ = ["diff", "-r", "-N", "-u", first_path, second_path]
    _v_ = _v_ >> subprocess_shell.start(cwd=base_path)
    return _v_ >> subprocess_shell.read(bytes=True, return_codes=(0, 1))


def _git_apply(diff_bytes, arguments, path, modifying=False, **kwargs):
    _v_ = _git(["apply"] + arguments + ["-"], path, modifying=modifying, **kwargs)
    return _v_ >> subprocess_shell.write(diff_bytes)


def _color_stat(process):
    process.get_subprocess().stdin.close()

    for line_string in process.get_stdout_lines():
        match = re.search(r"\d+\s+(?P<plus>\+*)(?P<minus>-*)\s*$", line_string)

        _v_ = match is None
        if _v_ or (len(match.group("plus")) == 0 and len(match.group("minus")) == 0):
            mygit.print(line_string, end="")
            continue

        mygit.print(line_string[: match.start("plus")], end="")
        mygit.print(match.group("plus"), end="", style="green")
        mygit.print(match.group("minus"), end="", style="red")
        mygit.print(line_string[match.end("minus") :], end="")

    return process


def _git(arguments, path, modifying=False, **kwargs):
    arguments = ["git"] + arguments
    return (
        mygit._process(arguments, cwd=path, **kwargs)
        if modifying
        else (arguments >> subprocess_shell.start(cwd=path, **kwargs))
    )


if __name__ == "__main__":
    typer.run(main)
