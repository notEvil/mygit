Another one of those "my something", when will it stop

# mygit

is a collection of shortcuts or alternative UIs for *Git*, most notably [stagetool](#stagetool).

```

 Usage: mygit.py [OPTIONS] COMMAND [ARGS]...

╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --install-completion        [bash|zsh|fish|powershell|p  Install completion for the  │
│                             wsh]                         specified shell.            │
│                                                          [default: None]             │
│ --show-completion           [bash|zsh|fish|powershell|p  Show completion for the     │
│                             wsh]                         specified shell, to copy it │
│                                                          or customize the            │
│                                                          installation.               │
│                                                          [default: None]             │
│ --help                                                   Show this message and exit. │
╰──────────────────────────────────────────────────────────────────────────────────────╯
╭─ Commands ───────────────────────────────────────────────────────────────────────────╮
│ clean-stash     Drop stashed changes with a convenient UI                            │
│ fetch           Fetch commits                                                        │
│ find-commits    Find commits similar to the working directory                        │
│ free-space      Free space by deleting unchanged files in the working directory      │
│ occupy-space    Undo `mygit free-space`                                              │
│ recover-commit  Recover lost commits                                                 │
│ reset           Reset HEAD or any combination of branch head, index and working      │
│                 directory                                                            │
│ st              Alias for stagetool                                                  │
│ stagetool       With this Python script you can edit the index (staged changes) and  │
│                 working tree of a Git repository using your favorite diff tool.      │
│ sw              Alias for switch                                                     │
│ switch          Switch to another branch                                             │
│ sync            Push/pull changes to/from linked directory                           │
│ track           Track/untrack files with a convenient UI                             │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```

[[_TOC_]]

## Quickstart

- Create virtual environment (optional but recommended)
  - e.g. install [*Pipenv*](https://github.com/pypa/pipenv) with `python -m pip install pipenv`
- Install *mygit* as Python package
  - e.g. `python -m pipenv run pip install -e .[all]`
- Run *mygit*
  - e.g. `python -m pipenv run mygit --help`

## switch

`mygit sw {branch}`

short for `git stash`, `git switch {branch}`, `git stash pop`.
It associates changes with branches and uses the stash as storage instead of carrying the current changes to the target branch.

```

 Usage: mygit.py switch [OPTIONS] [BRANCH]

 Switch to another branch

╭─ Arguments ──────────────────────────────────────────────────────────────────────────╮
│   branch      [BRANCH]                                                               │
╰──────────────────────────────────────────────────────────────────────────────────────╯
╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --detach  -d        Detach HEAD                                                      │
│ --help              Show this message and exit.                                      │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```

### Details

**Process**

1. If there are (staged) changes, stash them
   - if there are already stashed changes for this branch, abort
2. Switch branch
3. If there are stashed changes for this branch, unstash them

- If "Switch branch" fails, just try again
- To restore changes, omit `{branch}`
- If `git` fails, for any reason, it will stop immediately and let you resolve the issue

### Motivation

With `git switch {branch}` you can switch to another branch.
Unless the working tree contains changes that require an explicit merge.
The command fails and you are left with a few options.
Either

- start the merge with `git switch {branch} -m`
- or stash changes with `git stash`
- or do something else that lets you switch, like commit the changes.

If you decide to merge, you have to perform the merge because there is no way to abort it.
This becomes a problem when you accidentally switch to the wrong branch.
To get to the right branch, you have to perform the merge, then switch to the right branch and most likely perform the merge again.
The same is true if you want to switch back to the original branch.
Needless to say, this is a terrible user experience.

If you decide to stash the changes, you usually get what you signed up for.
Unless there are staged changes and you forget to use `--index`; then they are gone.
And if you frequently switch branches, managing the stash becomes tiresome and error-prone.

And last but not least, doing something else is tedious if you just want to switch temporarily.

Assuming that branches are sufficiently different, it is rather likely to face this issue and merging is not the preferred way to deal with it.

## stagetool

`mygit st`

similar to `git difftool -d` with the commit, index (staged changes) and working tree in a three-way diff.
You can directly edit the index, and the working tree if necessary.
Changes are applied after confirmation.

By default, it uses [*Meld*](http://meldmerge.org/) which is amazing imo.

There are many options, see `mygit stagetool --help`

```

 Usage: mygit.py stagetool [OPTIONS]

 With this Python script you can edit the index (staged changes) and working tree of a
 Git repository using your favorite diff tool.
 By default, it creates the directories
 - "working": w
 -   "index": _
 -  "commit": c
 in a temporary directory and starts Meld with "commit" to the left, "index" in the
 middle and "working" to the right. When Meld is closed, it validates the changes and
 waits for confirmation to apply.
 With --staged/--cached: shows the directories "commit", "index" and "current index"
 (i)
 With --working: shows the directories "current index" (i), "index" and "working"
 With --two-way: doesn't show the directory "current index" (i)
 With --no-symlinks: copies files to "working"
 With --update-working (default): symlinks files to "current working" (W) instead,
 copies files to "working" and eventually validates and applies changes to the working
 tree
 With --command: uses `command.format("/path/to/left", "/path/to/middle",
 "/path/to/right")`. If the command doesn't change, appends the paths.
 With --reverse_order: shows the directories in reverse order

╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --staged,--cached      --no-staged,--no-cached          [default: no-staged]         │
│ --working              --no-working                     Like --staged                │
│                                                         [default: no-working]        │
│ --two-way              --no-two-way                     Without "current index" (i). │
│                                                         Requires either --staged or  │
│                                                         --working                    │
│                                                         [default: no-two-way]        │
│ --symlinks             --no-symlinks                    [default: symlinks]          │
│ --update-working       --no-update-working              Explicitly update working    │
│                                                         tree. Preferred over just    │
│                                                         --symlinks                   │
│                                                         [default: update-working]    │
│ --command                                         TEXT  Command format string        │
│                                                         [default: meld]              │
│ --dir-diff         -d  --no-dir-diff                    [default: dir-diff]          │
│ --reverse-order        --no-reverse-order               Show directories in reverse  │
│                                                         order, e.g. "working" |      │
│                                                         "index" | "commit"           │
│                                                         [default: no-reverse-order]  │
│ --track                --no-track                       Run `mygit track` in         │
│                                                         preparation                  │
│                                                         [default: track]             │
│ --help                                                  Show this message and exit.  │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```

### Motivation

The following is my personal experience and I might have missed obvious solutions to the problems I faced.
If so, please let me know by creating an issue so I can update this part accordingly.
Thanks!

I always struggled to be efficient at staging changes for commit.
Probably because I tend to have a dirty working tree with code in comments used for debugging and personal notes.
So I can't just run `git add -u` before commit and found `git add -i` to be very useful, with three exceptions.

1. Small mistakes

If I detect small mistakes while staging individual hunks, there is no easy way to fix them right there.
Either quit staging, fix them in the working tree, start staging and skip to the hunks.
Or edit the hunks in unified diff format and adjust the working tree later.
Both options are equally bad.

2. Accidental stage

If I stage a hunk by accident, then unstaging that hunk is surprisingly difficult.
`git reset -p` operates in the opposite direction (from commit to index) which is confusing, especially when editing hunks in unified diff format.

3. Final validation

Eventually, I tend to validate the index using `git difftool -d` and `git difftool -d --staged`.
This is tedious and always felt like operating with magnifying glasses.

## sync

`mygit sync`

provides synchronization from/to a linked repository. This serves a very specific purpose:

Lets assume you work on some repository at different locations using different devices and you frequently move between the locations.
Your working tree is never clean (e.g. contains build files, code for debugging, ...) and there are many or large untracked files.

- You don't want to check in everything
  - e.g. create a temporary branch, check in everything, push, move, fetch, checkout, switch to original branch, remove temporary branch
- You don't want to work directly on a network share
  - e.g. because of throughput/network load, latency, single point of failure, ...

With the repository on a network share and `./.git` symlinking to the corresponding directory, the command executes `git diff`, `git apply` and `git restore` such that the working set is transfered from one directory to the other.

`mygit sync /path/to/directory` links the directories and `mygit sync --stash {local|remote}` enables automatic stashing if both directories contain changes.

```

 Usage: mygit.py sync [OPTIONS] [PATH]

 Push/pull changes to/from linked directory

╭─ Arguments ──────────────────────────────────────────────────────────────────────────╮
│   path      [PATH]  Path used to initialize link to remote repository                │
╰──────────────────────────────────────────────────────────────────────────────────────╯
╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --stash                              [local|remote]  Which side to stash if diverged │
│ --backups                            INTEGER         Maximum number of backups       │
│                                                      [default: 3]                    │
│ --find-commit    --no-find-commit                    Use `mygit find-commits         │
│                                                      --exact` to identify false dirt │
│                                                      [default: find-commit]          │
│ --show           --no-show                           Just show, don't execute        │
│                                                      [default: no-show]              │
│ --help                                               Show this message and exit.     │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```

## reset

`mygit reset {target}`

provides shortcuts for resetting HEAD, branch head, index and the working tree.

**WARNING: potentially destructive!**

`git reset` by default resets HEAD, branch head and index.
With `--soft` it won't touch the index and with `--hard` it resets the working tree as well.
In all cases, the operation is potentially destructive as it moves the branch head.

`mygit reset` by default resets HEAD and only resets the branch head, index and working tree if requested.
It is also possible to reset any combination, e.g. just the working tree.
Just like `git reset`, it will silently discard changes!

```

 Usage: mygit.py reset [OPTIONS] SOURCE

 Reset HEAD or any combination of branch head, index and working directory

╭─ Arguments ──────────────────────────────────────────────────────────────────────────╮
│ *    source      TEXT  [required]                                                    │
╰──────────────────────────────────────────────────────────────────────────────────────╯
╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --branch     --no-branch       Reset branch HEAD [default: no-branch]                │
│ --index      --no-index        Reset index [default: no-index]                       │
│ --working    --no-working      Reset working directory [default: no-working]         │
│ --help                         Show this message and exit.                           │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```

## find-commits

`mygit find-commits`

finds commits that match the current working tree.

If, for any reason, the working tree is based on a different commit than HEAD, it may be difficult to find that commit.

```

 Usage: mygit.py find-commits [OPTIONS]

 Find commits similar to the working directory

╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --scores              --no-scores                With scores [default: no-scores]    │
│ --exact               --no-exact                 Only exact matches                  │
│                                                  [default: no-exact]                 │
│ --referenced          --no-referenced            Only referenced commits             │
│                                                  [default: no-referenced]            │
│ --include-stash       --no-include-stash         Consider stash                      │
│                                                  [default: no-include-stash]         │
│ --head-shortcut       --no-head-shortcut         If HEAD matches exactly, stop       │
│                                                  immediatly                          │
│                                                  [default: head-shortcut]            │
│ --deleted-shortcut    --no-deleted-shortcut      If any file doesn't exist in        │
│                                                  working directory, ignore commit    │
│                                                  [default: deleted-shortcut]         │
│ --subset-shortcut     --no-subset-shortcut       If there is another commit with a   │
│                                                  larger set of exactly matching      │
│                                                  files, ignore commit                │
│                                                  [default: subset-shortcut]          │
│ --help                                           Show this message and exit.         │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```

## fetch

`mygit fetch`

provides shortcuts for fetching commits.

This complements my personal workflow and might not fit yours.

- `mygit fetch [OPTIONS] {url}` is short for `git clone ...` with `--depth {number}`.
It resets the configuration `remote.origin.fetch` so future operations work just like they would on a deep clone.

- `mygit fetch [OPTIONS]` is short for `git fetch ...` of the tracked branch with `--deepen={number}`.
If the branch doesn't exist, it will use remote of the current branch and `--depth={number}`.

- `mygit fetch --tags` just fetches all tags using remote of the current branch.

```

 Usage: mygit.py fetch [OPTIONS] [URL]

 Fetch commits

╭─ Arguments ──────────────────────────────────────────────────────────────────────────╮
│   url      [URL]  URL used to clone repository                                       │
╰──────────────────────────────────────────────────────────────────────────────────────╯
╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --branch  -b               TEXT                                                      │
│ --number  -n               INTEGER  Number of commits to fetch [default: 10]         │
│ --tags        --no-tags             Fetch tags [default: no-tags]                    │
│ --remote                   TEXT     [default: None]                                  │
│ --help                              Show this message and exit.                      │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```

## clean-stash

`mygit clean-stash`

is a simple UI to show and optionally drop stashed changes.

```

 Usage: mygit.py clean-stash [OPTIONS]

 Drop stashed changes with a convenient UI

╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --keep-mysync    --no-keep-mysync      Keep stashed changes added by mygit           │
│                                        [default: keep-mysync]                        │
│ --help                                 Show this message and exit.                   │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```

## recover-commit

`mygit recover-commit`

is a simple UI to show unreachable commits and create a branch.

```

 Usage: mygit.py recover-commit [OPTIONS]

 Recover lost commits

╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --full-hash    --no-full-hash      Show full commit hashes [default: no-full-hash]   │
│ --help                             Show this message and exit.                       │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```

## track

`mygit track`

is a simple UI to track/untrack files.

```

 Usage: mygit.py track [OPTIONS]

 Track/untrack files with a convenient UI

╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --help          Show this message and exit.                                          │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```

## free-space

`mygit free-space`

frees storage space occupied by unchanged and untracked files.

If you are running out of storage space and have inactive Git repositories with many unchanged or untracked files, chances are, you can reclaim a lot of space by deleting those files.
This operation is reversible with `mygit occupy-space`.

```

 Usage: mygit.py free-space [OPTIONS]

 Free space by deleting unchanged files in the working directory
 Undo with `mygit occupy-space`.

╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --help          Show this message and exit.                                          │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```

## occupy-space

`mygit occupy-space`

restores files deleted by `mygit free-space`.

```

 Usage: mygit.py occupy-space [OPTIONS]

 Undo `mygit free-space`

╭─ Options ────────────────────────────────────────────────────────────────────────────╮
│ --help          Show this message and exit.                                          │
╰──────────────────────────────────────────────────────────────────────────────────────╯
```
